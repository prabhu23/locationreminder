package com.locationreminder.persentation.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.locationreminder.core.TrackerReminderService;
import com.locationreminder.domin.model.Reminder;
import com.locationreminder.domin.repository.ReminderRepository;
import com.locationreminder.persentation.viewmodel.ReminderViewModel;
import com.locationreminder.util.Constants;

public class ReminderListAdapter extends ListAdapter<Reminder, ReminderViewHolder> {
private ReminderViewModel reminderViewModel;
public ReminderListAdapter(ReminderViewModel reminderViewModel,   @NonNull DiffUtil.ItemCallback<Reminder> diffCallback) {
        super(diffCallback);
        this.reminderViewModel =reminderViewModel;
        }

@Override
public ReminderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ReminderViewHolder.create(parent);
        }

@Override
public void onBindViewHolder(ReminderViewHolder holder, int position) {
        Reminder current = getItem(position);

        holder.bind(current);
    holder.loactionItemDelete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), TrackerReminderService.class);
            intent.setAction(Constants.ACTION_START_OR_RESUME_SERVICE);
            intent.putExtra("remider_name",  current.getReminderName());
            intent.putExtra("remider_time",  current.getReminderTime());
            intent.putExtra("remider_Location",  current.getDestinationLocationLat()+","+current.getDestinationLocationLon());
            v.getContext().startService(intent);
        }
    });
        }

public static class LocationDiff extends DiffUtil.ItemCallback<Reminder> {

    @Override
    public boolean areItemsTheSame(@NonNull Reminder oldItem, @NonNull Reminder newItem) {
        return oldItem == newItem;
    }

    @Override
    public boolean areContentsTheSame(@NonNull Reminder oldItem, @NonNull Reminder newItem) {
        return oldItem.getDestinationLocationLat().equals(newItem.getDestinationLocationLat());
    }
}
}
