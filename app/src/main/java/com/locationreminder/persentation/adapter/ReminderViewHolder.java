package com.locationreminder.persentation.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.locationreminder.R;
import com.locationreminder.domin.model.Reminder;

public class ReminderViewHolder extends RecyclerView.ViewHolder {
    private final TextView loactionNameItemView;
    private final TextView loactionItemViewLat;
    private final TextView loactionItemViewLon;
    private final TextView loactionItemReminderTime;
    public final Button loactionItemDelete;

    private ReminderViewHolder(View itemView) {
        super(itemView);
        loactionNameItemView = itemView.findViewById(R.id.reminder_name);
        loactionItemViewLat = itemView.findViewById(R.id.reminder_location_lat);
        loactionItemViewLon= itemView.findViewById(R.id.reminder_location_lon);
        loactionItemReminderTime= itemView.findViewById(R.id.reminder_time);
        loactionItemDelete =itemView.findViewById(R.id.start_bt);
    }

    public void bind(Reminder reminder) {
        loactionNameItemView.setText(reminder.getReminderName());
        loactionItemViewLat.setText(""+reminder.getDestinationLocationLat());
        loactionItemViewLon.setText(""+reminder.getDestinationLocationLon());
        loactionItemReminderTime.setText(reminder.getReminderTime() +"Min");
    }

    static ReminderViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reminder_item, parent, false);
        return new ReminderViewHolder(view);
    }
}