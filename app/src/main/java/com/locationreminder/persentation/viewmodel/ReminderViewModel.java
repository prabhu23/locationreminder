package com.locationreminder.persentation.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.locationreminder.data.repository.ReminderRespositoryImp;
import com.locationreminder.domin.model.Reminder;
import com.locationreminder.domin.repository.ReminderRepository;
import com.locationreminder.domin.use_case.AddReminderUseCase;
import com.locationreminder.domin.use_case.DeleteReminderUseCase;
import com.locationreminder.domin.use_case.GetReminderListUseCase;

import java.util.HashMap;
import java.util.List;

public class ReminderViewModel  extends AndroidViewModel {

    private ReminderRepository mRepository;
    private WorkManager mWorkManager;
    private final LiveData<List<Reminder>> mAllReminder;
    public ReminderViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ReminderRespositoryImp(application);
        mAllReminder = GetReminderListUseCase.getReminder(mRepository);
        mWorkManager = WorkManager.getInstance(application);
    }

    public void insertAppReminder(Reminder reminder){
        AddReminderUseCase.addReminder(mRepository, reminder);
    }

    public LiveData<List<Reminder>> getmAllReminder(){
        return mAllReminder;
    }

    @SuppressLint("RestrictedApi")
    public void deleteRemiderById(Reminder reminder){
        HashMap workObject = new HashMap();
        workObject.put("reminderId", reminder);
        workObject.put("repository", mRepository);

        Data.Builder builder = new Data.Builder();
        builder.put("reminder", reminder);
        builder.put("repository", mRepository);

        OneTimeWorkRequest deleteRequest =
                new OneTimeWorkRequest.Builder(DeleteReminderUseCase.class)
                        .setInputData(builder.build())
                        .build();

        mWorkManager.enqueue(deleteRequest);

    }

}
