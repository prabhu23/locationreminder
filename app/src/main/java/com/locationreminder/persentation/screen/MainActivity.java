package com.locationreminder.persentation.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.locationreminder.R;
import com.locationreminder.databinding.ActivityDestinationMapsBinding;
import com.locationreminder.databinding.ActivityMainBinding;
import com.locationreminder.persentation.adapter.ReminderListAdapter;
import com.locationreminder.persentation.viewmodel.ReminderViewModel;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private ReminderViewModel reminderViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        reminderViewModel = new ViewModelProvider(this).get(ReminderViewModel.class);

        FloatingActionButton fab = binding.getRoot().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent destinationMap = new Intent(MainActivity.this, DestinationMapsActivity.class);
                startActivity(destinationMap);
            }
        });

        RecyclerView recyclerView =  binding.getRoot().findViewById(R.id.reminder_list);
        final ReminderListAdapter adapter = new ReminderListAdapter(reminderViewModel, new ReminderListAdapter.LocationDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        reminderViewModel.getmAllReminder().observe(this, reminders -> {
            // Update the cached copy of the words in the adapter.
            adapter.submitList(reminders);
        });
    }
}