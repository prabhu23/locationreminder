package com.locationreminder.domin.repository;

import androidx.lifecycle.LiveData;

import com.locationreminder.domin.model.Reminder;

import java.util.List;

public interface ReminderRepository {
    public void insertReminder(Reminder reminder);
    public LiveData<List<Reminder>> getAllReminder();
    public boolean deleteReminder(Long id);
}
