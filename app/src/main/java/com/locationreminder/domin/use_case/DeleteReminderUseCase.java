package com.locationreminder.domin.use_case;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.locationreminder.domin.model.Reminder;
import com.locationreminder.domin.repository.ReminderRepository;

import java.util.Map;

public class DeleteReminderUseCase extends Worker {
    private  ReminderRepository reminderRepository;
    private Reminder reminder;
    public DeleteReminderUseCase(
            @NonNull Context appContext,
            @NonNull WorkerParameters workerParams ){
        super(appContext, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        Map inputData =getInputData().getKeyValueMap();
        Reminder reminder = (Reminder) inputData.get("reminder");
        ReminderRepository reminderRepository = (ReminderRepository) inputData.get("repository");
        return  reminderRepository.deleteReminder(reminder.getId())? Result.success() : Result.failure();
    }
}
