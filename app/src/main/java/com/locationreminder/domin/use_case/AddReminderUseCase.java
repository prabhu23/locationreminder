package com.locationreminder.domin.use_case;

import com.locationreminder.domin.model.Reminder;
import com.locationreminder.domin.repository.ReminderRepository;

public class AddReminderUseCase {
    public static void addReminder(ReminderRepository reminderRepository, Reminder reminder){
        reminderRepository.insertReminder(reminder);
    }
}
