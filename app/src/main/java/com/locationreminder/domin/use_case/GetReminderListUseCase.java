package com.locationreminder.domin.use_case;

import androidx.lifecycle.LiveData;

import com.locationreminder.domin.model.Reminder;
import com.locationreminder.domin.repository.ReminderRepository;

import java.util.List;

public class GetReminderListUseCase {
    public static LiveData<List<Reminder>> getReminder(ReminderRepository reminderRepository){
        return reminderRepository.getAllReminder();
    }
}
