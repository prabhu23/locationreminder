package com.locationreminder.domin.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;


@Entity(tableName = "reminder")
public class Reminder {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @NonNull
    private String reminderName;

    @NonNull
    private String destinationLocationLat;

    @NonNull
    private String destinationLocationLon;

    @NonNull
    private int reminderTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NonNull
    public String getReminderName() {
        return reminderName;
    }

    public void setReminderName(@NonNull String reminderName) {
        this.reminderName = reminderName;
    }

    @NonNull
    public String getDestinationLocationLat() {
        return destinationLocationLat;
    }

    public void setDestinationLocationLat(@NonNull String destinationLocationLat) {
        this.destinationLocationLat = destinationLocationLat;
    }
    @NonNull
    public String getDestinationLocationLon() {
        return destinationLocationLon;
    }

    public void setDestinationLocationLon(@NonNull String destinationLocationLon) {
        this.destinationLocationLon = destinationLocationLon;
    }

    public int getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(int reminderTime) {
        this.reminderTime = reminderTime;
    }
}
