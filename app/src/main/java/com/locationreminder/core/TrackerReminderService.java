package com.locationreminder.core;

import static com.locationreminder.util.Constants.ACTION_PAUSE_SERVICE;
import static com.locationreminder.util.Constants.ACTION_START_OR_RESUME_SERVICE;
import static com.locationreminder.util.Constants.ACTION_STOP_SERVICE;
import static com.locationreminder.util.Constants.FASTEST_LOCATION_INTERVAL;
import static com.locationreminder.util.Constants.LOCATION_UPDATE_INTERVAL;
import static com.locationreminder.util.Constants.NOTIFICATION_CHANNEL_ID;
import static com.locationreminder.util.Constants.NOTIFICATION_CHANNEL_NAME;
import static com.locationreminder.util.Constants.NOTIFICATION_ID;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.LifecycleService;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;
import com.locationreminder.R;
import com.locationreminder.domin.model.Reminder;
import com.locationreminder.persentation.screen.MainActivity;
import com.locationreminder.util.Constants;

public class TrackerReminderService extends LifecycleService {
    private static final String TAG = "TrackerReminderService";
    Boolean isFirstRun = true;
    MutableLiveData<LatLng> currentLocation = new MutableLiveData<LatLng>();
    MutableLiveData<LatLng> destnationLocation = new MutableLiveData<LatLng>();
    String name;
    int time;
    int stopServiceTracker =0;
    boolean firstNotification = true;
    boolean secondNotification = true;

    MutableLiveData<Boolean> isTracking = new MutableLiveData<Boolean>(false);
    FusedLocationProviderClient fusedLocationProviderClient;

    NotificationCompat.Builder baseNotificationBuilder;

    NotificationCompat.Builder curNotificationBuilder;

    @Override
    public void onCreate() {
        super.onCreate();
        curNotificationBuilder = baseNotificationBuilder;
        fusedLocationProviderClient= new  FusedLocationProviderClient(this);
        isTracking.observe(this, aBoolean -> {
            updateLocationTracking(aBoolean);
        });
        firstNotification = true;
        secondNotification = true;
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        if(intent!=null){
            switch (intent.getAction()){
                case  ACTION_START_OR_RESUME_SERVICE:
                    firstNotification = true;
                    secondNotification = true;
                    name = intent.getExtras().getString("remider_name");
                    time = intent.getExtras().getInt("remider_time");
                    String loaction =  intent.getExtras().getString("remider_Location");
                    Double lat = Double.parseDouble(loaction.substring(0, loaction.indexOf(",")));
                    Double lng =  Double.parseDouble(loaction.substring(loaction.indexOf(",")+1,loaction.length()));
                    LatLng destLatLng = new LatLng( lat, lng);
                    destnationLocation.postValue(destLatLng);
                    Log.i(TAG, "ACTION_START_OR_RESUME_SERVICE: "+destLatLng+"::"+time+"::"+name);
                    if(isFirstRun){
                        startFrogroundService( );
                        isFirstRun = false;
                    }
                    break;
                case  ACTION_PAUSE_SERVICE:
                    Log.i(TAG, "ACTION_PAUSE_SERVICE: ");
                    break;
                case  ACTION_STOP_SERVICE:
                    Log.i(TAG, "ACTION_PAUSE_SERVICE: ");
                    break;
                default:
                    break;
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }
    @SuppressLint("MissingPermission")
    private void updateLocationTracking(Boolean isTracking) {
        Log.i(TAG, "updateLocationTracking: "+isTracking);
        if (isTracking) {
                LocationRequest request = new LocationRequest()
                        .setInterval(LOCATION_UPDATE_INTERVAL)
                        .setFastestInterval(FASTEST_LOCATION_INTERVAL)
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                fusedLocationProviderClient.requestLocationUpdates(
                        request,
                        locationCallback,
                        Looper.getMainLooper()
                );
        } else {
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    LocationCallback locationCallback = new LocationCallback(){
        int i=0;
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
           if(isTracking.getValue()){
                currentLocation.postValue(new LatLng(locationResult.getLocations().get(0).getLatitude(),locationResult.getLocations().get(0).getLongitude()));
            }

        }
    };

    private  void startFrogroundService(){
        isTracking.postValue(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        createNotificationChannel(notificationManager);
        PendingIntent pendingService = PendingIntent.getService(this, 2, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder baseNotificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentTitle("You have started your trip to your "+name)
                .setContentIntent(pendingService);
        curNotificationBuilder = baseNotificationBuilder;
        startForeground(NOTIFICATION_ID, baseNotificationBuilder.build());

        currentLocation.observe(this,latLng -> {
            if(latLng.latitude != 0.0) {

                Location startPoint=new Location("locationA");
                startPoint.setLatitude(latLng.latitude );
                startPoint.setLongitude(latLng.longitude);

                Location endPoint=new Location("locationA");
                endPoint.setLatitude(destnationLocation.getValue().latitude);
                endPoint.setLongitude(destnationLocation.getValue().longitude);

                double distance=convertMeterToKilometer( startPoint.distanceTo(endPoint));
                Log.i(TAG, "startFrogroundService:distance "+distance);
                if( time == 5) {
                    if (distance < 1) {
                        stopServiceTracker++;
                        if (stopServiceTracker > 5) {
                            isTracking.postValue(false);
                            updateLocationTracking(isTracking.getValue());
                            stopForeground(true);
                            stopSelf();
                        }
                    } else if (distance < 3 && secondNotification) {
                        secondNotification = false;
                        NotificationCompat.Builder notificationNew = curNotificationBuilder
                                .setContentText("Your will reach your " + name + " in " + time + " Mins");
                        notificationManager.notify(NOTIFICATION_ID, notificationNew.build());

                    } else if (distance < 5 && firstNotification) {
                        firstNotification = false;
                        NotificationCompat.Builder notification = curNotificationBuilder
                                .setContentText("Your are " + distance + "Km away from your " + name);
                        notificationManager.notify(NOTIFICATION_ID, notification.build());

                    }
                }else if( time == 10){
                    if (distance < 1) {
                        stopServiceTracker++;
                        if (stopServiceTracker > 5) {
                            isTracking.postValue(false);
                            updateLocationTracking(isTracking.getValue());
                            stopForeground(true);
                            stopSelf();
                        }
                    } else if (distance < 6 && secondNotification) {
                        secondNotification = false;
                        NotificationCompat.Builder notificationNew = curNotificationBuilder
                                .setContentText("Your will reach your " + name + " in " + time + " Mins");
                        notificationManager.notify(NOTIFICATION_ID, notificationNew.build());

                    } else if (distance < 10 && firstNotification) {
                        firstNotification = false;
                        NotificationCompat.Builder notification = curNotificationBuilder
                                .setContentText("Your are " + distance + "Km away from your " + name);
                        notificationManager.notify(NOTIFICATION_ID, notification.build());

                    }
                }else if( time == 15){
                    if (distance < 1) {
                        stopServiceTracker++;
                        if (stopServiceTracker > 5) {
                            isTracking.postValue(false);
                            updateLocationTracking(isTracking.getValue());
                            stopForeground(true);
                            stopSelf();
                        }
                    } else if (distance < 9 && secondNotification) {
                        secondNotification = false;
                        NotificationCompat.Builder notificationNew = curNotificationBuilder
                                .setContentText("Your will reach your " + name + " in " + time + " Mins");
                        notificationManager.notify(NOTIFICATION_ID, notificationNew.build());

                    } else if (distance < 15 && firstNotification) {
                        firstNotification = false;
                        NotificationCompat.Builder notification = curNotificationBuilder
                                .setContentText("Your are " + distance + "Km away from your " + name);
                        notificationManager.notify(NOTIFICATION_ID, notification.build());

                    }
                }
            }
        });
    }


    private void createNotificationChannel(NotificationManager notificationManager){
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH);
        notificationManager.createNotificationChannel(notificationChannel);
    }

    public static double convertMeterToKilometer(float meter) {
        return round((float) (meter * 0.001),2);
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
