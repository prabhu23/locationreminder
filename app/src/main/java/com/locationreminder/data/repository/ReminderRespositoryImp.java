package com.locationreminder.data.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.locationreminder.data.data_source.ReminderDao;
import com.locationreminder.data.data_source.ReminderDatabase;
import com.locationreminder.domin.model.Reminder;
import com.locationreminder.domin.repository.ReminderRepository;

import java.util.List;

public class ReminderRespositoryImp implements ReminderRepository {


    private ReminderDao reminderDao;
    private LiveData<List<Reminder>> reminLiveData;

    public ReminderRespositoryImp(Application application){
        ReminderDatabase db = ReminderDatabase.getDatabase(application);
        reminderDao = db.reminderDao();
        reminLiveData = reminderDao.getReminderList();
    }

    @Override
    public void insertReminder(Reminder reminder) {
        ReminderDatabase.databaseWriteExecutor.execute(() -> {
            reminderDao.insert(reminder);
        });
    }

    @Override
    public LiveData<List<Reminder>> getAllReminder() {
        return reminLiveData;
    }

    @Override
    public boolean deleteReminder(Long id) {
        return   reminderDao.deleteById(id)==0?false:true;
    }
}
