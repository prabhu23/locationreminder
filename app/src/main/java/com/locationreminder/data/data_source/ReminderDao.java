package com.locationreminder.data.data_source;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.locationreminder.domin.model.Reminder;

import java.util.List;

@Dao
public interface ReminderDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Reminder reminder);

    @Query("DELETE FROM reminder WHERE id = :id")
    int deleteById(long id);

    @Query("SELECT * FROM reminder ")
    LiveData<List<Reminder>> getReminderList();
}
